-- Create a new user with the username "rentaluser" and the password "rentalpassword".
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE your_database_name TO rentaluser;
COMMIT;

-- Grant "rentaluser" SELECT permission for the "customer" table.
GRANT SELECT ON TABLE customer TO rentaluser;
COMMIT;

-- Check the permission
SELECT * FROM customer;
COMMIT;

-- Create a new user group called "rental" and add "rentaluser" to the group.
CREATE ROLE rental;
GRANT rental TO rentaluser;
COMMIT;

-- Grant the "rental" group INSERT and UPDATE permissions for the "rental" table.
GRANT INSERT, UPDATE ON TABLE rental TO rental;
COMMIT;

-- Insert a new row
INSERT INTO rental (column1, column2, ...) VALUES (value1, value2, ...);
COMMIT;

-- Update an existing row
UPDATE rental SET column1 = new_value WHERE condition;
COMMIT;

-- Revoke the "rental" group's INSERT permission for the "rental" table.
REVOKE INSERT ON TABLE rental FROM rental;
COMMIT;

-- Try to insert, this should result in a permission error
INSERT INTO rental (column1, column2, ...) VALUES (value1, value2, ...);
COMMIT;

-- Create a personalized role for customers with non-empty payment and rental history.
DO $$ 
DECLARE 
    customer_role_name text;
BEGIN
    FOR customer_record IN 
        SELECT customer_id, first_name, last_name
        FROM customer
        WHERE payment_history IS NOT NULL AND rental_history IS NOT NULL
    LOOP
        customer_role_name := 'client_' || customer_record.first_name || '_' || customer_record.last_name;
        
        -- Create a role
        CREATE ROLE IF NOT EXISTS customer_role_name;
        
        -- Grant access to own data in rental and payment tables
        GRANT SELECT ON TABLE payment TO customer_role_name;
        GRANT SELECT ON TABLE rental TO customer_role_name;
        
        -- Assign the role to the user
        GRANT customer_role_name TO rentaluser;

        -- Add user-specific restrictions if necessary
        ALTER DEFAULT PRIVILEGES FOR ROLE client_role IN SCHEMA public
        GRANT SELECT ON TABLE payment TO rentaluser;
        GRANT SELECT ON TABLE rental TO rentaluser;        
        COMMIT;
    END LOOP;
END $$;
COMMIT;
